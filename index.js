/**
 * @format
 */

import { Navigation } from "react-native-navigation";
import App from './App';

import Home from './src/Home'
import More from './src/More'
import Activity from './src/Activity'
import Messages from './src/Messages'
import Matches from './src/Matches'
import Profile from './src/Profile'
import Account from './src/Account'
import SignUp from './src/SignUp'
import SignUp2 from './src/SignUp2'
import Loading from './src/Loading'
import DeviceInfo from 'react-native-device-info';
import firebase from 'react-native-firebase'
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import Mail from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

Navigation.registerComponent(`navigation.playground.WelcomeScreen`, () => App);
Navigation.registerComponent(`Home`, () => Home);
Navigation.registerComponent(`More`, () => More);
Navigation.registerComponent(`Activity`, () => Activity);
Navigation.registerComponent(`Messages`, () => Messages);
Navigation.registerComponent(`Matches`, () => Matches);
Navigation.registerComponent(`Profile`, () => Profile);
Navigation.registerComponent(`Account`, () => Account);
Navigation.registerComponent(`SignUp`, () => SignUp);
Navigation.registerComponent(`SignUp2`, () => SignUp2);
Navigation.registerComponent(`Loading`, () => Loading);


Navigation.events().registerAppLaunchedListener(() => {
     Navigation.setRoot({
         root: {
              component: {
                    name: "SignUp2"
                  }
           }
      });
    });



