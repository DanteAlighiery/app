import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, ScrollView, Image, TouchableOpacity} from 'react-native';
import firebase from 'react-native-firebase'
import {Navigation} from "react-native-navigation";
import Icon from 'react-native-vector-icons/EvilIcons';



export default class Activity extends Component {
	constructor(props) {
		super(props);
		this.state = {
			females: [],
			popular: [],
		};
	}
	componentDidMount() {
		const documentRef = firebase.firestore().collection('female-users');
		documentRef.get();
		documentRef.onSnapshot((querySnapshot) => {
			const females = [];
			querySnapshot.forEach((doc) => {
				females.push({
					key: doc.id,
					...doc.data()
				});
			});
			this.setState({
				females,
			});
		});}
	toDetail(female) {
		return Navigation.push(this.props.componentId, {
			component: {
				name: "Profile",
				passProps: {
					...female
				}
			}
		})

	}

	render() {
		return (
			<ScrollView contentContainerStyle={styles.container}>
				<View style={styles.viewed}><Text style={styles.viewtext}>Viewed Me</Text></View>
				<View style={styles.viewed}><Text style={styles.viewtext}>I Viewed</Text></View>
				{this.state.females.map(female =>
						<TouchableOpacity onPress={() => this.toDetail(female)} key={this.state.females.key} style={styles.profile}>
							<View style={styles.card}>
								<Image resizeMode={'cover'} source={{uri: female.photo}} style={styles.image}/>
								<View style={styles.wtt}>
									<View style={styles.wt}>
										<Text style={styles.name}>
											{female.name}
										</Text>
										<Text style={styles.name}> </Text>
										<Text style={styles.age}>
											{female.age}
										</Text></View>
									<View style={styles.wt}>
										<Text style={styles.city}>{female.country}</Text>
										<Text style={styles.city}>, </Text>
										<Text style={styles.city}>{female.city}</Text>
									</View>
								</View>
								<View style={{alignItems: 'flex-end', width:"50%", flex:1}}>
									<Icon name="heart" size={40} color="blue" style={{paddingTop:15, paddingRight:1}}/></View>
							</View>
						</TouchableOpacity>
					)}
				<View style={styles.viewed}><Text style={styles.viewtext}>Mutual Likes</Text></View>
				<View style={styles.viewed}><Text style={styles.viewtext}>My Likes</Text></View>
				<View style={{marginBottom:20, width:'100%', height:10,}}></View>
			</ScrollView>

		);

	}
}

const styles = StyleSheet.create({
	card:{
		flexDirection:'row',
		width:'100%',
		backgroundColor:'white',
         height:80,
		padding:10,
		borderBottomWidth:1,
		borderColor: 'gray',

	},
	container: {
		backgroundColor:'#eae9e8',
		alignItems: 'flex-start',
		flexWrap: 'wrap',
		width:'100%',
		justifyContent: 'flex-start',
	},
	image: {
		width: 50,
		height: 50,
        marginRight:10,
		borderTopLeftRadius:5,
		borderTopRightRadius:5,
		borderColor:'black',
		borderWidth:0.4
	},
	profile:{
		width:'100%',
		padding:0,


	},
viewed:{
		backgroundColor: 'white',
	shadowColor: "#000",
	shadowOffset: {
		width: 0,
		height: 2,
	},
	shadowOpacity: 0.25,
	shadowRadius: 3.84,
	elevation: 2,
	width:'100%',
	height: 67,
	alignItems: 'flex-start',
	justifyContent: 'center',
	marginTop:10,
},
	viewtext:{
     fontSize:22,
		color:'black',
		paddingLeft:15,
	},
	name:{
		color: 'lightblue',
		fontSize: 18,
	},
	age:{
		color: 'black',
		fontSize: 18,
		marginLeft:3,
	},
	wt:{
	flexDirection:'row'
	},
	wwt:{
		flexDirection:'column',
		width:'50%',
		padding:5,
		flex:1,
	},
	city:{
		fontSize:15,
		color:'gray',
	},
});