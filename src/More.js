import React, {Component} from 'react';
import { StyleSheet, Text, View, Button, ScrollView, Image, TouchableOpacity} from 'react-native';
import firebase from 'react-native-firebase'
import {Navigation} from "react-native-navigation";
import MaterialIcon from "react-native-vector-icons/MaterialIcons";
import Pencil from "react-native-vector-icons/MaterialCommunityIcons";
import Icon from 'react-native-vector-icons/AntDesign';

export default class More extends Component {
	constructor(props) {
		super(props);
		this.state = {
			females: [],
			popular: [],
		};
	}




	render() {
		return (
			<ScrollView contentContainerStyle={styles.container}>
				<View style={styles.msges}>
					<TouchableOpacity style={styles.button}><Text style={styles.txtbtn}>Get Full Access - Subscribe Now</Text></TouchableOpacity>
					<View style={styles.midle}>
					<TouchableOpacity><Pencil name={"pencil-circle"} size={30} color={'black'}  style={{marginLeft:105, paddingBottom:3, position:'absolute'}}/>
						<MaterialIcon name={"account-circle"} size={150} lineHeight={2} color="gray" style={{}} /></TouchableOpacity></View>
					<Text style={{fontSize: 22, color:'black', paddingBottom:10}}>Max, 24</Text>
				</View>
				<View style={styles.menu}>
					<View style={{width:'100%',flexDirection: 'row', justifyContent:'space-between',alignItems:'baseline', height:60 }}>
					<TouchableOpacity style={styles.bar}><Text style={styles.mentxt}>Discovery Preference</Text>
						<View style={styles.another}>
						<Icon name="right" size={25} color="black" style={{}}/>
						</View>
					</TouchableOpacity>
					</View>
					<View style={{width:'100%',flexDirection: 'row', justifyContent:'space-between',alignItems:'baseline', height:60 }}>
						<TouchableOpacity style={styles.bar}><View style={styles.half}><Text style={styles.mentxt}>Profile Display Settings</Text></View>
						<View style={styles.another}>
						<Icon name="right" size={25} color="black" style={{}}/>
						</View>
					</TouchableOpacity>
					</View>
					<View style={{width:'100%',flexDirection: 'row', justifyContent:'space-between',alignItems:'baseline', height:60 }}>
						<TouchableOpacity style={styles.bar}><View style={styles.half}><Text style={styles.mentxt}>Account Settings</Text></View>
						<View style={styles.another}>
						<Icon name="right" size={25} color="black" style={{}}/>
						</View>
					</TouchableOpacity>
					</View>
					<View style={{width:'100%',flexDirection: 'row', justifyContent:'space-between',alignItems:'baseline', height:60 }}>
						<TouchableOpacity style={styles.bar}><View style={styles.half}><Text style={styles.mentxt}>Logout</Text></View>
						<View style={styles.another}>
						<Icon name="right" size={25} color="black" style={{}}/>
						</View>
					</TouchableOpacity>
					</View>
				</View>
				<View style={styles.menu2}>
					<View style={{width:'100%',flexDirection: 'row', justifyContent:'space-between',alignItems:'baseline', height:60 }}>
						<TouchableOpacity style={styles.bar}><View style={styles.half}><Text style={styles.mentxt}>Help</Text></View>
							<View style={styles.another}>
								<Icon name="right" size={25} color="black" style={{}}/>
							</View>
						</TouchableOpacity>
					</View>
					<View style={{width:'100%',flexDirection: 'row', justifyContent:'space-between',alignItems:'baseline', height:60 }}>
						<TouchableOpacity style={styles.bar}><View style={styles.half}><Text style={styles.mentxt}>Believe</Text></View>
							<View style={styles.another}>
								<Icon name="right" size={25} color="black" style={{}}/>
							</View>
						</TouchableOpacity>
					</View>
					<View style={{width:'100%',flexDirection: 'row', justifyContent:'space-between',alignItems:'baseline', height:60 }}>
						<TouchableOpacity style={styles.bar}><View style={styles.half}><Text style={styles.mentxt}>About</Text></View>
							<View style={styles.another}>
								<Icon name="right" size={25} color="black" style={{}}/>
							</View>
						</TouchableOpacity></View></View>

					</ScrollView>
		);}


}




const styles = StyleSheet.create({
	container: {
		backgroundColor:'#eae9e8',

		flexWrap: 'wrap',
		width:'100%',



	},
	midle:{
		marginTop:25,
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom:0,
	},
		msges:{
		backgroundColor: 'white',
		alignItems: 'center',
		justifyContent: 'flex-start',
		width:'100%',
		height:300,
			shadowColor: "#000",
			shadowOffset: {
				width: 0,
				height: 3,
			},
			shadowOpacity: 0.27,
			shadowRadius: 4.65,

			elevation: 3,

	},
	txtbtn:{
		color:'white',
		fontSize:17,
		borderColor:'black',

	},
	button:{
		backgroundColor:'#595959',
		width:'92%',
		height:45,
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius:4,
		borderColor:'black',
		borderWidth:0.6,
		marginTop: 25,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 3,
		},
		shadowOpacity: 0.27,
		shadowRadius: 4.65,

		elevation: 6,
	},
	menu:{
        marginTop:15,

		width:'100%',
		backgroundColor:'white',
	},
	bar:{
		width:'100%',
		height:55,
		alignItems: 'center',
		justifyContent: 'space-between',
		backgroundColor:'white',
		flexDirection: 'row',
		borderBottomWidth:0.5,
		borderColor:'darkgray',
	},
	mentxt:{
     marginLeft:10,
		color:'black',
		fontSize:19,
	},
	half:{
		flex:2
	},
	another:{
        flex:1,
		alignItems:'flex-end',
		marginRight:5,
	},
	menu2:{
		marginTop:15,
        height:200,
		width:'100%',
		backgroundColor:'white',
	},
});