import React, {Component} from 'react'
import { StyleSheet, Text, TextInput, View, TouchableOpacity } from 'react-native'
import { Navigation } from "react-native-navigation";
import firebase from 'react-native-firebase'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {Input} from './components/Input';
import RNPickerSelect from 'react-native-picker-select';
import DatePicker from './react-native-datepicker'
import Geolocation from '@react-native-community/geolocation';

const polll = [
	{
		label: 'Man',
		value: 'Man'
	},
	{
		label: 'Woman',
		value: 'Woman'
	}
];
export default class SignUp2 extends Component {
	state = {email: '', password: '', errorMessage: null,firstName:'', lastName:'', coordinates:{}};
	inputRefs = {
		gender: null
	};

	async getPosition(){
		 Geolocation.getCurrentPosition( async info => {
			try {
				let resp = await fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${info.coords.latitude},${info.coords.longitude}&key=AIzaSyBXDf91UvoO7Ql23rz7908EL4mGmpFfM-8`);
				let respJson = await resp.json();
				console.log(respJson.results);
				console.log(respJson)
			}catch(error) {
				console.log('error');
				this.setState({x: "error"});
				return error
			}
		});
	}

	render() {
		const placeholder = {
			label: 'Gender',
			value: null,
			color: '#9EA0A4'
		};
		return (
			<KeyboardAwareScrollView>
				<View style={styles.container}>
					<Text style={{fontSize: 22, fontFamily: 'Circe-Bold', color: 'black'}}>Sign Up</Text>
					{this.state.errorMessage &&
					<Text style={{color: 'red'}}>
						{this.state.errorMessage}
					</Text>}
					<View style={styles.inputs}>
						<Input
							ref={this.props.ref}
							onFocus={this.onFocusChange}
							onBlur={this.onBlur}
							autoCapitalize="none"
							onChangeText={this.props.onChangeText}
							onSubmitEditing={this.props.onSubmitEditing}
							returnKeyType={this.props.returnKeyType}
							value={this.props.value}
							title={'First Name'}
						/><Input
							ref={this.props.ref}
							onFocus={this.onFocusChange}
							onBlur={this.onBlur}
							autoCapitalize="none"
							onChangeText={this.props.onChangeText}
							onSubmitEditing={this.props.onSubmitEditing}
							returnKeyType={this.props.returnKeyType}
							value={this.props.lastName}
							title={'Last Name'}
						/>
						<View style={styles.picker}><RNPickerSelect
							placeholder={placeholder}
							items={polll}
							onValueChange={value => {
								this.setState({
									gender: value
								});
							}}
							placeholderTextColor={'gray'}
							style={pickerSelectStyles}
							value={this.state.gender}
							useNativeAndroidPickerStyle={false}
							ref={el => {
								this.inputRefs.gender = el;
							}}/></View>
						<View style={styles.picker}><DatePicker
							date={this.state.date}
							mode='date'
							placeholder='Birthday'
							format='YYYY-MM-DD'
							minDate='1950-01-01'
							maxDate='2016-06-01'
							confirmBtnText='Принять'
							cancelBtnText='Отмена'
							showIcon={false}
							customStyles={{
								dateInput: {
									borderWidth: 0,
									padding: 0,
									margin: 0,
								},
								placeholderText: {
									fontSize: 20,
									marginLeft: 10,
								},
								dateText: {
									fontSize: 20,
									marginLeft: 10,
								}
							}}
							onDateChange={(date) => {
								this.setState({date: date})
							}}
						/></View><View style={styles.picker}>
						<TouchableOpacity onPress={()=> this.getPosition()}><Text style={{fontSize:20}}>Geoposition</Text></TouchableOpacity></View>
					</View>
					<TouchableOpacity style={styles.button} ><Text
						style={styles.text}>Next</Text></TouchableOpacity>
				</View>
			</KeyboardAwareScrollView>

		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'flex-start',
		alignItems: 'center',
		marginTop:40,
	},
	textInput: {
		height: 40,
		width: '85%',
		borderColor: 'gray',
		borderBottomWidth: 2,
		marginTop: 8
	},
	inputs:{
		paddingHorizontal:1,
		width: '100%',
		alignItems: 'center',
		marginTop:10,
	},
	button:{
		backgroundColor:'gray',
		marginTop:230,
		width:'85%',
		height:50,
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius:7,
		padding: 5,
	},
	text:{
		fontFamily: 'Circe-Regular',
		fontSize:22,
		color:'white',
		padding:5,
	},
	formInput: {
		borderBottomWidth: 1.5,
		borderColor: '#333',
		height: 60,width: '85%',
	},
	input: {
		borderWidth: 0

	},
	picker:{
		width:'85%',
		borderBottomWidth:2,
		borderColor:'gray',
		marginTop:15,
		marginBottom:5,

	}
});
const pickerSelectStyles = StyleSheet.create({
	inputIOS: {
		fontSize: 18,
		paddingVertical: 0,
		paddingHorizontal: 10,
		borderWidth: 0,
		borderRadius: 10,
		color: 'black',
		height: 56,
	},
	inputAndroid: {
		fontSize: 20,
		paddingHorizontal: 10,
		paddingVertical: 8,
		borderBottomWidth: 0,
		borderColor: 'gray',
		color: 'black',
		marginTop: 5,
		marginLeft: 2,
		width:'100%'

	}
});