import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, ScrollView, Image, TouchableOpacity} from 'react-native';
import firebase from 'react-native-firebase'
import {Navigation} from "react-native-navigation";




export default class Messages extends Component {
	constructor(props) {
		super(props);
		this.state = {
			catalog: [],
			popular: [],
		};
	}




	render() {
		return (
			<ScrollView contentContainerStyle={styles.container}>
			<View style={styles.msges}>
			<Text style={{fontSize: 21, color:'black', padding:5}}>No messages yet</Text>
			<Text style={{fontSize: 14.5, color:'black', paddingBottom:10}}>Browse our members & start a conversation</Text>
			<TouchableOpacity style={styles.button}><Text style={styles.txtbtn}>Browse Now</Text></TouchableOpacity>
			</View>
			</ScrollView>

		);}



}
const styles = StyleSheet.create({
container: {
	backgroundColor:'#eae9e8',
		flexDirection: 'row',
		alignItems: 'flex-start',
		flexWrap: 'wrap',

		width:'100%',
		height:"100%",
		justifyContent: 'flex-start',

},
	msges:{
	backgroundColor: 'white',
		alignItems: 'center',
		justifyContent: 'center',
		width:'100%',
		height:250,
	},
	txtbtn:{
     color:'white',
		fontSize:22,
		borderColor:'black',

	},
	button:{
	backgroundColor:'#595959',
		width:'92%',
		height:50,
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius:4,
		borderColor:'black',
		borderWidth:0.6,
		marginTop: 10,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 3,
		},
		shadowOpacity: 0.27,
		shadowRadius: 4.65,

		elevation: 6,
	},

});