import React, {Component} from 'react';
import {TextInput, View, StyleSheet} from 'react-native';
var FloatingLabel = require('react-native-floating-labels');

export class Input extends Component {
	state = {
		focus: this.props.autofocus
	};
	onFocusChange = () => {
		this.setState({ isFocused: true });
	};
	onBlur = () => {
		this.setState({ isFocused: false});
	};
	render() {
		return (
			<View style={[styles.container, this.props.style, this.state.focus ? styles.focused : null]}>
				<FloatingLabel
					ref={this.props.ref}
					onFocus={this.onFocusChange}
					onBlur={this.onBlur}
					autoCapitalize="none"
					onChangeText={this.props.onChangeText}
					onSubmitEditing={this.props.onSubmitEditing}
					returnKeyType={this.props.returnKeyType}
					value={this.props.value}
					labelStyle={(this.state.isFocused) ? {
						color: 'orange',
					}:{
						color: 'gray',
					}}
					inputStyle={styles.input}
					style={(this.state.isFocused) ? {padding:1,width: '85%',borderColor: 'orange',borderBottomWidth: 2,}
						: {padding:1,width: '85%',borderColor: 'gray',borderBottomWidth: 2,}}
				>{this.props.title}</FloatingLabel>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
			paddingHorizontal:1,
			width: '100%',
			alignItems: 'center',
			marginTop:0,
		},

	focused: {
		borderWidth: 2,
		borderColor: '#AF9778'
	},
	input: {
		borderWidth: 0,
		marginBottom:-5,
		height:50

	},
});