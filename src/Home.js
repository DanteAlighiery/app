import React, {Component} from 'react';
import { StyleSheet, Text, View, Button, ScrollView, Image, TouchableOpacity} from 'react-native';
import firebase from 'react-native-firebase'
import {Navigation} from "react-native-navigation";
import { ScrollableTabView,  ScrollableTabBar,DefaultTabBar } from '@valdio/react-native-scrollable-tabview'

export default class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			females: [],
			popular: [],
		};
	}

	componentDidMount() {
		const documentRef = firebase.firestore().collection('female-users');
		documentRef.get();
		documentRef.onSnapshot((querySnapshot) => {
			const females = [];
			querySnapshot.forEach((doc) => {
				females.push({
					key: doc.id,
					...doc.data()
				});
			});
			this.setState({
				females,
			});
		});}

	toDetail(female) {
		return Navigation.push(this.props.componentId, {
			component: {
				name: "Profile",
				passProps: {
					...female
				}
			}
		})

	}



		render() {
			return (

				<ScrollableTabView
					style={styles.container}
					renderTabBar={() => <ScrollableTabBar tabBarUnderlineStyle={'red'}  style={{height:10, marginTop: 100}} backgroundColor='rgba(255, 255, 255, 0.7)'/>}
					tabBarPosition='overlayBottom'  tabBarBackgroundColor='white' tabBarActiveTextColor='black' tabBarTextStyle={{fontFamily:'Circe-Regular'}}
				>
					<ScrollView tabLabel='New' contentContainerStyle={styles.container}>
					<View style={styles.profiles}>
					{this.state.females.map(female =>

						<TouchableOpacity onPress={() => this.toDetail(female)} key={this.state.females.key} style={styles.profile}>
							<View style={styles.imtx}><View style={styles.card}>
							<Image resizeMode={'cover'} source={{uri: female.photo}} style={styles.image}/>
							<View style={styles.wtt}>
							<View style={styles.wt}>
							<Text style={styles.name}>
								{female.name}
							</Text>
								<Text style={styles.name}>, </Text>
								<Text style={styles.age}>
									{female.age}
								</Text></View>
								<View style={styles.wt} >
								<Text style={styles.city}>{female.country}</Text>
								<Text style={styles.city}>, </Text>
								<Text style={styles.city}>{female.city}</Text>
									<View/>
								</View>
							</View></View>
								<View style={styles.relig}>
									<Text style={styles.religion}>{female.religion}</Text>
									<Text style={styles.religion}>, </Text>
									<Text style={styles.religion}>{female.activity}</Text>
								</View>
							</View>
						</TouchableOpacity>
					)}
					</View>
					</ScrollView>
					<ScrollView tabLabel='Online' contentContainerStyle={styles.container}>
						<View style={styles.profiles}>
							{this.state.females.map(female =>

								<TouchableOpacity onPress={() => this.toDetail(female)} key={this.state.females.key} style={styles.profile}>
									<View style={styles.imtx}><View style={styles.card}>
										<Image resizeMode={'cover'} source={{uri: female.photo}} style={styles.image}/>
										<View style={styles.wtt}>
											<View style={styles.wt}>
												<Text style={styles.name}>
													{female.name}
												</Text>
												<Text style={styles.name}>, </Text>
												<Text style={styles.age}>
													{female.age}
												</Text></View>
											<View style={styles.wt} >
												<Text style={styles.city}>{female.country}</Text>
												<Text style={styles.city}>, </Text>
												<Text style={styles.city}>{female.city}</Text>
												<View/>
											</View>
										</View></View>
										<View style={styles.relig}>
											<Text style={styles.religion}>{female.religion}</Text>
											<Text style={styles.religion}>, </Text>
											<Text style={styles.religion}>{female.activity}</Text>
										</View>
									</View>
								</TouchableOpacity>
							)}
						</View>
					</ScrollView>
					<ScrollView tabLabel='Distance' contentContainerStyle={styles.container}>
						<View style={styles.profiles}>
							{this.state.females.map(female =>

								<TouchableOpacity onPress={() => this.toDetail(female)} key={this.state.females.key} style={styles.profile}>
									<View style={styles.imtx}><View style={styles.card}>
										<Image resizeMode={'cover'} source={{uri: female.photo}} style={styles.image}/>
										<View style={styles.wtt}>
											<View style={styles.wt}>
												<Text style={styles.name}>
													{female.name}
												</Text>
												<Text style={styles.name}>, </Text>
												<Text style={styles.age}>
													{female.age}
												</Text></View>
											<View style={styles.wt} >
												<Text style={styles.city}>{female.country}</Text>
												<Text style={styles.city}>, </Text>
												<Text style={styles.city}>{female.city}</Text>
												<View/>
											</View>
										</View></View>
										<View style={styles.relig}>
											<Text style={styles.religion}>{female.religion}</Text>
											<Text style={styles.religion}>, </Text>
											<Text style={styles.religion}>{female.activity}</Text>
										</View>
									</View>
								</TouchableOpacity>
							)}
						</View>
					</ScrollView>
					<ScrollView tabLabel='Match %' contentContainerStyle={styles.container}>
						<View style={styles.profiles}>
							{this.state.females.map(female =>

								<TouchableOpacity onPress={() => this.toDetail(female)} key={this.state.females.key} style={styles.profile}>
									<View style={styles.imtx}><View style={styles.card}>
										<Image resizeMode={'cover'} source={{uri: female.photo}} style={styles.image}/>
										<View style={styles.wtt}>
											<View style={styles.wt}>
												<Text style={styles.name}>
													{female.name}
												</Text>
												<Text style={styles.name}>, </Text>
												<Text style={styles.age}>
													{female.age}
												</Text></View>
											<View style={styles.wt} >
												<Text style={styles.city}>{female.country}</Text>
												<Text style={styles.city}>, </Text>
												<Text style={styles.city}>{female.city}</Text>
												<View/>
											</View>
										</View></View>
										<View style={styles.relig}>
											<Text style={styles.religion}>{female.religion}</Text>
											<Text style={styles.religion}>, </Text>
											<Text style={styles.religion}>{female.activity}</Text>
										</View>
									</View>
								</TouchableOpacity>
							)}
						</View>
					</ScrollView>
					<ScrollView tabLabel='LookBook' contentContainerStyle={styles.container}>
						<View style={styles.profiles}>
							{this.state.females.map(female =>

								<TouchableOpacity onPress={() => this.toDetail(female)} key={this.state.females.key} style={styles.profile}>
									<View style={styles.imtx}><View style={styles.card}>
										<Image resizeMode={'cover'} source={{uri: female.photo}} style={styles.image}/>
										<View style={styles.wtt}>
											<View style={styles.wt}>
												<Text style={styles.name}>
													{female.name}
												</Text>
												<Text style={styles.name}>, </Text>
												<Text style={styles.age}>
													{female.age}
												</Text></View>
											<View style={styles.wt} >
												<Text style={styles.city}>{female.country}</Text>
												<Text style={styles.city}>, </Text>
												<Text style={styles.city}>{female.city}</Text>
												<View/>
											</View>
										</View></View>
										<View style={styles.relig}>
											<Text style={styles.religion}>{female.religion}</Text>
											<Text style={styles.religion}>, </Text>
											<Text style={styles.religion}>{female.activity}</Text>
										</View>
									</View>
								</TouchableOpacity>
							)}
						</View>
					</ScrollView>

				</ScrollableTabView>
			);

	}







}
const styles = StyleSheet.create({
    botbar:{
    	marginTop:0,
    },
	container: {
		backgroundColor:'#eae9e8',
		alignItems: 'flex-start',
		flexWrap: 'wrap',
		padding:3,
		width:'100%',
		alignSelf:'baseline',
		justifyContent: 'flex-start',

	},
	image: {
		width: '100%',
		height: '70%',
		flex:1,
		borderTopLeftRadius:5,
		borderTopRightRadius:5,
		borderColor:'black',
		borderWidth:0.4
	},
	profiles:{
		alignItems: 'flex-start',
		flexDirection:'row',
		width:'100%',
		flexWrap: 'wrap',
       height:'100%'
	},
	profile:{
		width:'50%',
		height:245,
		padding:5,

		shadowColor: "black",
		shadowOffset: {
			width: 10,
			height: 12,
		},
		shadowOpacity: 0.58,
		shadowRadius: 16.00,
		elevation: 5,
		borderTopLeftRadius:5,
		borderTopRightRadius:5,
		borderBottomLeftRadius:5,
		borderBottomRightRadius:5,

	},
	card:{
		flex: 1,
		height:140
	},
	item:{
		justifyContent: 'flex-start',
		width: '45%',
		height: 100,
		padding: 2,
		marginHorizontal: 5,
		flex:1,
	},
	name:{
		color:'white',
		fontSize:16,
		fontFamily:'Circe-Bold',
	},
	city:{
		fontSize:15,
		color:'white',
		fontFamily:'Circe-Bold',
	},
	age:{
		color:'white',
		fontSize:16,
		fontFamily:'Circe-Bold',
	},
	religion:{
    fontSize:14,
	color:'black',
		fontFamily:'Circe-Regular',
	},
	wt:{
		paddingLeft:5,
		flexDirection:'row',

		justifyContent: 'flex-end',
	},
	wtt:{
		justifyContent: 'flex-end',
		position:'absolute',
		height:'100%',
		alignItems:'flex-start',
		padding:1,

	},
	imtx:{
		height: 50,
		width:'100%',
		flex:1,
		justifyContent: 'flex-end',
	},
	relig:{
		backgroundColor:"white",
		height:50,
		flexDirection:'row',
		width:'100%',
		flexWrap: 'wrap',
		paddingLeft:5,
		paddingTop:3,
		borderBottomLeftRadius:5,
		borderBottomRightRadius:5,
	}
});