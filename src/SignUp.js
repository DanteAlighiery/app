import React, {Component} from 'react'
import { StyleSheet, Text, TextInput, View, TouchableOpacity } from 'react-native'
import { Navigation } from "react-native-navigation";
import firebase from 'react-native-firebase'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import FloatLabelTextInput from 'react-native-floating-label-text-input';
var FloatingLabel = require('react-native-floating-labels');

export default class SignUp extends Component {
	state = { email: '', password: '', errorMessage: null, isFocused: false, isPinokused: false };

	handleSignUp = () => {
		const user = firebase.auth().currentUser;
		firebase
			.auth()
			.createUserWithEmailAndPassword(this.state.email, this.state.password)
			.then(() => Navigation.push(this.props.componentId, {
					component: {
						name: "SignUp2",
				}})
			)
			.then( () =>firebase.firestore().collection('users').doc(user.uid).set({
				password:this.state.password,
				email:this.state.email,
			}))
			.catch(error => this.setState({ errorMessage: error.message }))
	};

	onFocusChange = () => {
		this.setState({ isFocused: true });
	};
	onBlur = () => {
		this.setState({ isFocused: false});
	};
	onPinocusChange = () => {
		this.setState({ isPinokused: true });
	};
	onPinBlur = () => {
		this.setState({ isPinokused: false});
	};

	render() {
		return (
			<KeyboardAwareScrollView>
			<View style={styles.container}>
				<Text style={{fontSize:22, fontFamily:'Circe-Bold', color:'black'}}>Sign Up</Text>
				{this.state.errorMessage &&
				<Text style={{ color: 'red' }}>
					{this.state.errorMessage}
				</Text>}
				<View style={styles.inputs}>
				<FloatingLabel
					ref={'_email'}
					onFocus={this.onFocusChange}
					onBlur={this.onBlur}

					autoCapitalize="none"
					onChangeText={email => this.setState({ email })}
					onSubmitEditing={() => this.refs._password.focus()}
					returnKeyType={'next'}
					value={this.state.email}
					labelStyle={(this.state.isFocused) ? {
						color: 'orange',
					}:{
						color: 'gray',
					}}
					inputStyle={styles.input}
					 style={(this.state.isFocused) ? {flex:1,paddingVertical:3,width: '85%',borderColor: 'orange',borderBottomWidth: 2,}
					 : {flex:1,padding:3,width: '85%',borderColor: 'gray',borderBottomWidth: 2,}}
				>Email</FloatingLabel>
				<FloatingLabel
				ref={'_password'}
				labelStyle={(this.state.isPinokused) ? {
					color: 'orange',
				}:{
					color: 'gray',
				}}
					secureTextEntry
					onFocus={this.onPinocusChange}
				onBlur={this.onPinBlur}
				autoCapitalize="none"
					returnKeyType={'go'}
					onSubmitEditing={() => this.handleSignUp()}
					style={(this.state.isPinokused) ? {padding:1,width: '85%',borderColor: 'orange',borderBottomWidth: 2,}
						: {padding:1,width: '85%',borderColor: 'gray',borderBottomWidth: 2,}}
					onChangeText={password => this.setState({ password })}
				inputStyle={styles.input}
					value={this.state.password}
				>Password</FloatingLabel></View>
				<TouchableOpacity style={styles.button} onPress={this.handleSignUp}><Text style={styles.text}>Next</Text></TouchableOpacity>
				<TouchableOpacity
					onPress={() => this.props.navigation.navigate('Login')}
				><Text style={{marginTop:5}}>Already have an account? Login</Text></TouchableOpacity>
			</View>
			</KeyboardAwareScrollView>
		)
	}

};
const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'flex-start',
		alignItems: 'center',
		marginTop:40,
	},
	textInput: {
		height: 40,
		width: '85%',
		borderColor: 'gray',
		borderBottomWidth: 2,
		marginTop: 8
	},
	inputs:{
		paddingHorizontal:1,
		width: '100%',
		alignItems: 'center',
		marginTop:10,
	},
	button:{
		backgroundColor:'gray',
		marginTop:230,
		width:'85%',
		height:50,
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius:7,
		padding: 5,
	},
	text:{
		fontFamily: 'Circe-Regular',
		fontSize:22,
		color:'white',
		padding:5,
	},
	formInput: {
		borderBottomWidth: 1.5,
		borderColor: '#333',
		height: 60,width: '85%',
	},
	input: {
		borderWidth: 0

		},
	});
