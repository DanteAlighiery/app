import React from 'react'
import { View, Text, ActivityIndicator, StyleSheet } from 'react-native'
import { Navigation } from "react-native-navigation";
import firebase from 'react-native-firebase'

export default class Loading extends React.Component {

	componentDidMount() {
		firebase.auth().onAuthStateChanged(user => {(user ? (Navigation.push(this.props.componentId, {
			component: {
				name: "Home",
			}
		})) : (Navigation.push(this.props.componentId, {
			component: {
				name: "SignUp",
				}
		})))
		})
	}

	render() {
		Navigation.setDefaultOptions({
			bottomTabs: {
				visible: false,
				drawBehind: true,
			}
		});
		return (
			<View style={styles.container}>
				<Text>Loading</Text>
				<ActivityIndicator size="large" />
			</View>
		)
	}
}
const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	}
});