import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, ScrollView, Image, TouchableOpacity} from 'react-native';
import firebase from 'react-native-firebase'
import {Navigation} from "react-native-navigation";
import Material from "react-native-vector-icons/MaterialCommunityIcons";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";




export default class Matches extends Component {
	constructor(props) {
		super(props);
		this.state = {
			females: [],
			popular: [],
		};
	}
	componentDidMount() {
		const documentRef = firebase.firestore().collection('female-users');
		documentRef.get();
		documentRef.onSnapshot((querySnapshot) => {
			const females = [];
			querySnapshot.forEach((doc) => {
				females.push({
					key: doc.id,
					...doc.data()
				});
			});
			this.setState({
				females,
			});
		});}


	render() {
		const female = this.state.females[2];
		return (

			 <ScrollView contentContainerStyle={styles.container}>
				 { female ? <View><View style={styles.img}>
					<Image resizeMode={'cover'} source={{uri: female.photo}} style={styles.image}/>
					<View style={styles.wtt}>
						<View style={styles.wt}>
							<Text style={styles.name}>
								{female.name}
							</Text>
							<Text style={styles.name}>, </Text>
							<Text style={styles.age}>
								{female.age}
							</Text></View>
						<View style={styles.wt}>
							<Text style={styles.city}>{female.country}</Text>
							<Text style={styles.city}>, </Text>
							<Text style={styles.city}>{female.city}</Text>
						</View></View></View>
				<View style={styles.description}><Text style={{fontFamily:'Circe-Regular', fontSize:15,color:'black'}}>{this.props.description}</Text></View>
				<View style={styles.about}>
					<View style={{flexDirection: 'row', width:'100%',}}>
						<View style={{flex:1}}>
							<Material name={"account-outline"} size={36} lineHeight={7} color="gray" style={{marginBottom:25, marginLeft:5}} />
						</View><View style={{flex:9, marginLeft:5}}><Text style={{fontFamily:'Circe-Regular', fontSize:15,color:'black'}}>{female.personal}</Text></View></View>
					<View style={{flexDirection: 'row', width:'100%',}}>
						<View style={{flex:1}}>
							<FontAwesome5 name={"cross"} size={30} lineHeight={7} color="gray" style={{marginBottom:25, marginLeft:5}} />
						</View><View style={{flex:9, marginLeft:5}}><Text style={{fontFamily:'Circe-Regular', fontSize:15,color:'black'}}>{female.religion}</Text></View></View>
					<View style={{flexDirection: 'row', width:'100%',}}>
						<View style={{flex:1}}>
							<Material name={"home-outline"} size={30} lineHeight={7} color="gray" style={{marginBottom:25, marginLeft:5}} />
						</View><View style={{flex:9, marginLeft:5}}><Text style={{fontFamily:'Circe-Regular', fontSize:15,color:'black'}}>{female.country}, {female.city}</Text></View></View>
					<View style={{flexDirection: 'row', width:'100%',}}>
						<View style={{flex:1}}>
							<SimpleLineIcons name={"flag"} size={30} lineHeight={7} color="gray" style={{marginBottom:25, marginLeft:5}} />
						</View><View style={{flex:9, marginLeft:5}}><Text style={{fontFamily:'Circe-Regular', fontSize:15,color:'black'}}>Never - drink, Non - smoker</Text></View></View>
					<View style={{flexDirection: 'row', width:'100%',}}>
						<View style={{flex:1}}>
							<SimpleLineIcons name={"emotsmile"} size={30} lineHeight={7} color="gray" style={{marginBottom:25, marginLeft:5}} />
						</View><View style={{flex:9, marginLeft:5}}><Text style={{fontFamily:'Circe-Regular', fontSize:15,color:'black'}}>{female.delight}</Text></View></View>

				</View></View> : null}</ScrollView>
		);

	}

}
const styles = StyleSheet.create({
	container: {
		backgroundColor:'#878281',

	},
	actionButtonIcon: {
		fontSize: 20,
		height: 22,
		color: 'white',
	},
	img:{
		width:'100%',
		height: 400,

	},
	image:{
		width:'100%',
		height: 400,
		position:'absolute'
	},
	about:{
		backgroundColor:'white',
		width:'100%',
		shadowColor: "black",
		shadowOffset: {
			width: 10,
			height: 12,
		},
		shadowOpacity: 0.58,
		shadowRadius: 16.00,
		elevation: 5,
	},

	name:{
		color:'white',
		fontSize:22,
		fontFamily:'Circe-Bold',
	},
	city:{
		fontSize:22,
		color:'white',
		fontFamily:'Circe-Bold',
	},
	age:{
		color:'white',
		fontSize:20,
		fontFamily:'Circe-Bold',
	},
	religion:{
		fontSize:14,
		color:'black',
		fontFamily:'Circe-Regular',
	},
	wt:{
		paddingLeft:5,
		flexDirection:'row',

		justifyContent: 'flex-end',
	},
	wtt:{
		justifyContent: 'flex-end',
		position:'absolute',
		height:'100%',
		alignItems:'flex-start',
		padding:1,

	},

	description:{
		backgroundColor:'white',
		width:'100%',
		alignSelf:'baseline',
		padding:10,
		shadowColor: "black",
		shadowOffset: {
			width: 10,
			height: 12,
		},
		shadowOpacity: 0.58,
		shadowRadius: 16.00,
		elevation: 5,
	}


});