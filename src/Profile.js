import React, {Component} from 'react';
import { StyleSheet, Text, View, Button, ScrollView, Image, TouchableOpacity} from 'react-native';
import firebase from 'react-native-firebase'
import {Navigation} from "react-native-navigation";
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcon from "react-native-vector-icons/MaterialIcons";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import Feather from "react-native-vector-icons/Feather";
import ActionButton from 'react-native-action-button';

export default class Profile extends Component {
	constructor(props) {
		super(props);
		this.state = {
			females: [],
			popular: [],
		};
	}

	componentDidMount() {
		const {
			name,
			activity,
			description,
			age,
			city,
			delight,
			find,
			photo,
			personal,
			religion,
		} = this.props;
		this.setState({
        female:{
	        name,
	        activity,
	        description,
	        age,
	        city,
	        delight,
	        find,
	        photo,
	        personal,
	        religion,
        }
		})
	}


	render() {
		return (
			<ScrollView contentContainerStyle={styles.container}>
				<View style={styles.img}>
					<Image resizeMode={'cover'} source={{uri: this.props.photo}} style={styles.image}/>
					<View style={styles.wtt}>
						<View style={styles.wt}>
							<Text style={styles.name}>
								{this.props.name}
							</Text>
							<Text style={styles.name}>, </Text>
							<Text style={styles.age}>
								{this.props.age}
							</Text></View>
						<View style={styles.wt}>
							<Text style={styles.city}>{this.props.country}</Text>
							<Text style={styles.city}>, </Text>
							<Text style={styles.city}>{this.props.city}</Text>
						</View></View></View>
				<View style={styles.description}><Text style={{fontFamily:'Circe-Regular', fontSize:15,color:'black'}}>{this.props.description}</Text></View>
				<View style={styles.about}>
					<View style={{flexDirection: 'row', width:'100%',}}>
						<View style={{flex:1}}>
						<Material name={"account-outline"} size={36} lineHeight={7} color="gray" style={{marginBottom:25, marginLeft:5}} />
						</View><View style={{flex:9, marginLeft:5}}><Text style={{fontFamily:'Circe-Regular', fontSize:15,color:'black'}}>{this.props.personal}</Text></View></View>
					<View style={{flexDirection: 'row', width:'100%',}}>
						<View style={{flex:1}}>
						<FontAwesome5 name={"cross"} size={30} lineHeight={7} color="gray" style={{marginBottom:25, marginLeft:5}} />
						</View><View style={{flex:9, marginLeft:5}}><Text style={{fontFamily:'Circe-Regular', fontSize:15,color:'black'}}>{this.props.religion}</Text></View></View>
					<View style={{flexDirection: 'row', width:'100%',}}>
						<View style={{flex:1}}>
						<Material name={"home-outline"} size={30} lineHeight={7} color="gray" style={{marginBottom:25, marginLeft:5}} />
						</View><View style={{flex:9, marginLeft:5}}><Text style={{fontFamily:'Circe-Regular', fontSize:15,color:'black'}}>{this.props.country}, {this.props.city}</Text></View></View>
					<View style={{flexDirection: 'row', width:'100%',}}>
						<View style={{flex:1}}>
						<SimpleLineIcons name={"flag"} size={30} lineHeight={7} color="gray" style={{marginBottom:25, marginLeft:5}} />
						</View><View style={{flex:9, marginLeft:5}}><Text style={{fontFamily:'Circe-Regular', fontSize:15,color:'black'}}>Never - drink, Non - smoker</Text></View></View>
                    <View style={{flexDirection: 'row', width:'100%',}}>
						<View style={{flex:1}}>
						<SimpleLineIcons name={"emotsmile"} size={30} lineHeight={7} color="gray" style={{marginBottom:25, marginLeft:5}} />
						</View><View style={{flex:9, marginLeft:5}}><Text style={{fontFamily:'Circe-Regular', fontSize:15,color:'black'}}>{this.props.delight}</Text></View></View>

				</View>
{/*<View>          <ActionButton buttonColor="rgba(231,76,60,1)">*/}
{/*				<ActionButton.Item buttonColor='blue' title="Message" onPress={() => {}}>*/}
{/*					<Feather name="mail" style={styles.actionButtonIcon} />*/}
{/*				</ActionButton.Item>*/}
{/*				<ActionButton.Item buttonColor='red' title="Like" onPress={() => {}}>*/}
{/*					<FontAwesome name="heart-o" style={styles.actionButtonIcon} />*/}
{/*				</ActionButton.Item>*/}
{/*				<ActionButton.Item buttonColor='yellow' title="Smile" onPress={() => {}}>*/}
{/*				<SimpleLineIcons name="emotsmile" style={styles.actionButtonIcon} />*/}
{/*				</ActionButton.Item>*/}
{/*                </ActionButton></View>*/}
			</ScrollView>);
	}


}


const styles = StyleSheet.create({
	container: {
		backgroundColor:'#878281',

	},
	actionButtonIcon: {
		fontSize: 20,
		height: 22,
		color: 'white',
	},
img:{
	width:'100%',
    height: 400,

},
image:{
	width:'100%',
	height: 400,
    position:'absolute'
	},
about:{
	backgroundColor:'white',
	width:'100%',
	shadowColor: "black",
	shadowOffset: {
		width: 10,
		height: 12,
	},
	shadowOpacity: 0.58,
	shadowRadius: 16.00,
	elevation: 5,
},

	name:{
		color:'white',
		fontSize:22,
		fontFamily:'Circe-Bold',
	},
	city:{
		fontSize:22,
		color:'white',
		fontFamily:'Circe-Bold',
	},
	age:{
		color:'white',
		fontSize:20,
		fontFamily:'Circe-Bold',
	},
	religion:{
		fontSize:14,
		color:'black',
		fontFamily:'Circe-Regular',
	},
	wt:{
		paddingLeft:5,
		flexDirection:'row',

		justifyContent: 'flex-end',
	},
	wtt:{
		justifyContent: 'flex-end',
		position:'absolute',
		height:'100%',
		alignItems:'flex-start',
		padding:1,

	},

description:{
	backgroundColor:'white',
	width:'100%',
	alignSelf:'baseline',
	padding:10,
	shadowColor: "black",
	shadowOffset: {
		width: 10,
		height: 12,
	},
	shadowOpacity: 0.58,
	shadowRadius: 16.00,
	elevation: 5,
}


});